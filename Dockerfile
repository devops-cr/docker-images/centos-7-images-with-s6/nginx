FROM centos:7-christos


COPY etc/yum.repos.d/ /etc/yum.repos.d/

RUN yum install -y nginx \
 && yum clean all

COPY etc/ /etc/

ARG HTTP_PORT=80 \
    HTTPS_PORT=443

ENV SSL_ENABLED=false \ 
    SECURITY_ENABLED=false \
    HTTP_PORT=$HTTP_PORT \
    HTTPS_PORT=$HTTPS_PORT \
    WORKERS=1 \
    WORKER_CONNECTIONS=1024

EXPOSE $HTTP_PORT $HTTPS_PORT
