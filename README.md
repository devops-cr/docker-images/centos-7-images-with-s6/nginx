# centos7-nginx

This is a docker image where we are creating an NginX webserver in our already created CentOS 7 image. We are download the latest nginx packages and that why we are installing the `nginx.repo` file. We provide the desired files to secure our webserver and ebable SSL if needed with the certificates we are passing to that image.

We have also created environment variables such as `HTTPS_PORT`, `HTTP_PORT`, `WORKERS` and `WORKER_CONNECTIONS` and we have set some default values. Depending on the system it is recommended to change the values of the `WORKERS` and `WORKER_CONNECTIONS`. 

In addition we have created two `true` or `false` flags to enable or disable security and SSL in our webserver. By default these values have been set to `false`.

## Usage Exanples.

A simple example where we are using the default values is the below one

    docker build -t nginx-test .
    docker run -d --name apache-default -p 8080:80 nginx-test

In this example we are build the image and we are publishing the container's port 80 to the hosts's port 8080. We are using the default values of the image so we don't have either SSL enabled or security configuration.

Another example where we use the environment variables is the following one

    docker build -t nginx-test2 .
    docker run -d --name nginx-safe -p 8080:80 -p 9090:443 -e SSL_ENABLED=true -e SECURITY_ENABLED=true -e WORKERS=10 nginx-test2

In this exampel we are creating a secure container with the `SSL_ENABLED` and `SECURITY_ENABLED` flags to true and we set `WORKERS` to 10.

Finally we are providing a `docker-compose.yml` file where we are creating a docker container and we have set the `SSL_ENABLED` and `SECURITY_ENABLED` flags to true. We need to be careful with the image name we are setting to the `docker-compose.yml` and change it to our needs if necessary. In our example we are using the `test2-nginx` image name

    docker build -t test2-nginx .
    docker-compose up -d
 
 

